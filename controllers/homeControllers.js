//@route   GET/
//@desc   Get home page
//@access Public

const getHomePage = (req, res) => {
    res.render('home', {
        hello: 'Home page',
        url: process.env.URL
    })
}

module.exports = {
    getHomePage: getHomePage
}
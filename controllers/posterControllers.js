const {addNewPosterToDB, getAllPosters} = require('../db/posters')
const { v4 } = require('uuid')

//@route   GET/
//@desc   Get all posters
//@access Public

const getPostersPage = async (req, res) => {
    const posters = await getAllPosters()
    console.log(posters)
    res.render('poster/posters', {
        title: 'posters title',
        posters,
        url: process.env.URL
    })
}


const addNewPosterPage = (req, res) => {
    res.render('poster/add-poster', {
        title: 'Changed title',
        url: process.env.URL
    })
}

const addNewPoster = async (req, res) => {
    const poster = {
        id: v4(),
        title: req.body.title,
        amount: req.body.amount,
        region: req.body.region,
        image: req.body.image,
        description: req.body.description,
    }
    await addNewPosterToDB({poster})
    res.redirect('/')

}

module.exports = {
    getPostersPage,
    addNewPosterPage,
    addNewPoster
}
const express = require('express')
const path = require('path')
const {engine} = require('express-handlebars');
const mongoose = require('mongoose');
const dotenv = require('dotenv')
const homeRoutes = require('./routes/homeRoutes')
const posterRoutes = require('./routes/posterRoutes')


dotenv.config()


const app = express()

app.use(express.json())
app.use(express.urlencoded({extended: false}))
app.use(express.static(path.join(__dirname, 'public')))

app.engine('.hbs', engine({extname: '.hbs'}))
app.set('view engine', '.hbs')

app.use('/', homeRoutes)
app.use('/posters', posterRoutes)

// const connectDB = async () => {
//     const connecting = await mongoose.connect('mongodb://localhost:27017/test',
//         {
//             useNewUrlParser: true,
//             useCreateIndex: true,
//             useFindAndModify: false,
//             useUnifiedTopology: true
//         })
//
//     console.log(`MongoDB connecto to: ${connecting.connection.host}`)
//
// }
//
// connectDB()

const PORT = process.env.PORT || 3000

app.listen(PORT, () => {
    console.log(`Sarver running on port: ${PORT}`)
})